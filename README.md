# exportDBmapper - QGIS plugin

[![pipeline status](https://gitlab.com/Oslandia/qgis/exportdbmapper/badges/master/pipeline.svg)](https://gitlab.com/Oslandia/qgis/exportdbmapper/-/commits/master)
[![documentation badge](https://img.shields.io/badge/documentation-autobuilt%20with%20Sphinx-blue)](https://oslandia.gitlab.io/qgis/exportdbmapper/)
[![pylint](https://oslandia.gitlab.io/qgis/exportdbmapper/lint/pylint.svg)](https://oslandia.gitlab.io/qgis/exportdbmapper/lint/)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)

## exportdbmapper

This is a plugin for QGIS to map export between PG database and GPKG file. It consists of a mapping editor and processings in the toolbox.

[:gb: Check-out the documentation](https://oslandia.gitlab.io/qgis/exportdbmapper/)
