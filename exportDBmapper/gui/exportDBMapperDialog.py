# -----------------------------------------------------------------
# Copyright (C) 2021 Oslandia
# Conctact : sebastien.peillet@oslandia.com ; contact@oslandia.com
# -----------------------------------------------------------------
# Licensed under the terms of GNU GPL 2
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# ---------------------------------------------------------------------

import os
import yaml

from PyQt5.QtWidgets import QDialog, QWidget, QVBoxLayout, QHBoxLayout, QTableWidgetItem, QLineEdit, QComboBox, QFileDialog
from PyQt5.QtCore import QSettings
from PyQt5 import uic
from qgis.gui import (
    QgsProviderConnectionComboBox,
    QgsDatabaseSchemaComboBox,
    QgsDatabaseTableComboBox,
)
from qgis.core import QgsApplication, QgsAuthMethodConfig

from .pgToGpkgWidget import PgToGpkgWidget
from .gpkgToPgWidget import GpkgToPgWidget


class ExportDBMapperDialog(QDialog):
    def __init__(self, iface):
        super(ExportDBMapperDialog, self).__init__()
        uic.loadUi(os.path.join(os.path.dirname(__file__), "exportDBMapperDialog.ui"), self)


        self.tabWidget.addTab(PgToGpkgWidget(),'PG to GPKG')
        self.tabWidget.addTab(GpkgToPgWidget(),'GPKG to PG')