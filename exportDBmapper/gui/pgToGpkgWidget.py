# -----------------------------------------------------------------
# Copyright (C) 2021 Oslandia
# Conctact : sebastien.peillet@oslandia.com ; contact@oslandia.com
# -----------------------------------------------------------------
# Licensed under the terms of GNU GPL 2
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# ---------------------------------------------------------------------

import os

import yaml
from PyQt5 import uic
from PyQt5.QtCore import QSettings
from PyQt5.QtWidgets import QComboBox, QFileDialog, QLineEdit, QTableWidgetItem, QWidget
from qgis.core import QgsApplication, QgsAuthMethodConfig, QgsDataSourceUri
from qgis.gui import (
    QgsDatabaseSchemaComboBox,
    QgsDatabaseTableComboBox,
    QgsProviderConnectionComboBox,
)

from .gpkgOutputSelector import GPKGOutputSelector


class PgToGpkgWidget(QWidget):
    def __init__(self):
        super(PgToGpkgWidget, self).__init__()
        uic.loadUi(os.path.join(os.path.dirname(__file__), "pgToGpkgWidget.ui"), self)

        self.addPushButton.clicked.connect(self.add_row)
        self.removePushButton.clicked.connect(self.remove_row)
        self.exportPushButton.clicked.connect(self.export_mapping)
        self.importPushButton.clicked.connect(self.import_mapping)

    def add_row(self):
        self.tableWidget.insertRow(self.tableWidget.rowCount())
        db_item = QgsProviderConnectionComboBox("postgres")
        self.tableWidget.setCellWidget(self.tableWidget.rowCount() - 1, 0, db_item)
        schema_item = QgsDatabaseSchemaComboBox("postgres", db_item.currentConnection())
        self.tableWidget.setCellWidget(self.tableWidget.rowCount() - 1, 1, schema_item)
        db_item.connectionChanged.connect(schema_item.setConnectionName)
        table_item = QgsDatabaseTableComboBox(
            "postgres", db_item.currentConnection(), schema_item.currentSchema()
        )
        self.tableWidget.setCellWidget(self.tableWidget.rowCount() - 1, 2, table_item)
        db_item.connectionChanged.connect(table_item.setConnectionName)
        schema_item.schemaChanged.connect(table_item.setSchema)

        gpkg_file_item = GPKGOutputSelector(self)
        self.tableWidget.setCellWidget(
            self.tableWidget.rowCount() - 1, 3, gpkg_file_item
        )
        gpkg_table_item = QLineEdit(table_item.currentTable())
        self.tableWidget.setCellWidget(
            self.tableWidget.rowCount() - 1, 4, gpkg_table_item
        )
        table_item.tableChanged.connect(gpkg_table_item.setText)
        if self.tableWidget.rowCount() == 1:
            self.tableWidget.resizeColumnsToContents()
        # set new row widget values from the last row values
        elif self.tableWidget.rowCount() > 1:
            connection = self.tableWidget.cellWidget(
                self.tableWidget.rowCount() - 2, 0
            ).currentConnection()
            db_item.setConnection(connection)
            schema = self.tableWidget.cellWidget(
                self.tableWidget.rowCount() - 2, 1
            ).currentSchema()
            schema_item.setSchema(schema)
            gpkg_file = self.tableWidget.cellWidget(
                self.tableWidget.rowCount() - 2, 3
            ).text()
            gpkg_file_item.setText(gpkg_file)

    def remove_row(self):
        self.tableWidget.removeRow(self.tableWidget.currentRow())

    def export_mapping(self):
        yaml_file, _ = QFileDialog.getSaveFileName(
            self, self.tr("YAML output"), filter="YAML (*.yaml)"
        )

        db_map = dict()
        gpkg_files = []
        for i in range(self.tableWidget.rowCount()):
            db_map["Table" + str(i)] = dict()
            connection = self.tableWidget.cellWidget(i, 0).currentConnection()
            db_map["Table" + str(i)]["connection_name"] = connection
            db_conn = QgsDataSourceUri(
                self.tableWidget.cellWidget(i, 0).currentConnectionUri()
            )
            if QSettings().value(
                "PostgreSQL/connections/{}/authcfg".format(connection), False
            ):
                db_conn.setUsername("")
                db_conn.setPassword("")
                db_conn.setAuthConfigId(
                    QSettings().value(
                        "PostgreSQL/connections/{}/authcfg".format(connection), False
                    )
                )
            db_map["Table" + str(i)]["db_conn"] = db_conn.uri(False)
            db_map["Table" + str(i)]["db_schema"] = self.tableWidget.cellWidget(
                i, 1
            ).currentSchema()
            db_map["Table" + str(i)]["db_table"] = self.tableWidget.cellWidget(
                i, 2
            ).currentTable()
            db_map["Table" + str(i)]["gpkg_file"] = self.tableWidget.cellWidget(
                i, 3
            ).text()
            db_map["Table" + str(i)]["gpkg_table"] = self.tableWidget.cellWidget(
                i, 4
            ).text()
            db_map["Table" + str(i)]["append_mode"] = (
                self.appendCheckBox.isChecked()
                if self.tableWidget.cellWidget(i, 3).text() not in gpkg_files
                else True
            )
            gpkg_files.append(self.tableWidget.cellWidget(i, 3).text())

        with open(yaml_file, "w") as file:
            data = yaml.dump(db_map, file)

    def import_mapping(self):
        file, _ = QFileDialog.getOpenFileName(
            self, self.tr("YAML mapping"), filter="YAML (*.yaml)"
        )

        if file:
            with open(file, "r") as yamlfile:
                data = yaml.load(yamlfile, Loader=yaml.FullLoader)

            if data:
                self.tableWidget.clearContents()
                for i, d in enumerate(data.values()):
                    if i == 0:
                        if d["append_mode"]:
                            self.appendCheckBox.setChecked(True)
                        else:
                            self.appendCheckBox.setChecked(False)
                    self.add_row()
                    db_item = self.tableWidget.cellWidget(
                        self.tableWidget.rowCount() - 1, 0
                    )
                    db_item.setCurrentIndex(db_item.findText(d["connection_name"]))
                    db_item.connectionChanged.emit(db_item.currentConnection())
                    schema_item = self.tableWidget.cellWidget(
                        self.tableWidget.rowCount() - 1, 1
                    )
                    schema_item.setSchema(d["db_schema"])
                    schema_item.schemaChanged.emit(schema_item.currentSchema())
                    table_item = self.tableWidget.cellWidget(
                        self.tableWidget.rowCount() - 1, 2
                    )
                    table_item.setTable(d["db_table"])
                    gpkg_file_item = self.tableWidget.cellWidget(
                        self.tableWidget.rowCount() - 1, 3
                    )
                    gpkg_file_item.setText(d["gpkg_file"])
                    gpkg_table_item = self.tableWidget.cellWidget(
                        self.tableWidget.rowCount() - 1, 4
                    )
                    gpkg_table_item.setText(d["gpkg_table"])
