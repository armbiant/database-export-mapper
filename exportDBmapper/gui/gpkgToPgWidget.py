# -----------------------------------------------------------------
# Copyright (C) 2021 Oslandia
# Conctact : sebastien.peillet@oslandia.com ; contact@oslandia.com
# -----------------------------------------------------------------
# Licensed under the terms of GNU GPL 2
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# ---------------------------------------------------------------------

import os

import yaml
from PyQt5 import uic
from PyQt5.QtCore import QSettings
from PyQt5.QtWidgets import QComboBox, QFileDialog, QLineEdit, QTableWidgetItem, QWidget
from qgis.core import (
    QgsApplication,
    QgsAuthMethodConfig,
    QgsDataSourceUri,
    QgsProviderConnectionModel,
)
from qgis.gui import (
    QgsDatabaseSchemaComboBox,
    QgsDatabaseTableComboBox,
    QgsProviderConnectionComboBox,
)

from .gpkgOutputSelector import GPKGOutputSelector


class GpkgToPgWidget(QWidget):
    def __init__(self):
        super(GpkgToPgWidget, self).__init__()
        uic.loadUi(os.path.join(os.path.dirname(__file__), "gpkgToPgWidget.ui"), self)

        self.addPushButton.clicked.connect(self.add_row)
        self.removePushButton.clicked.connect(self.remove_row)
        self.exportPushButton.clicked.connect(self.export_mapping)
        self.importPushButton.clicked.connect(self.import_mapping)

    def add_row(self):
        self.tableWidget.insertRow(self.tableWidget.rowCount())

        gpkg_file_item = QgsProviderConnectionComboBox("ogr")
        self.tableWidget.setCellWidget(
            self.tableWidget.rowCount() - 1, 0, gpkg_file_item
        )
        gpkg_table_item = QgsDatabaseTableComboBox(
            "ogr", gpkg_file_item.currentConnection()
        )
        self.tableWidget.setCellWidget(
            self.tableWidget.rowCount() - 1, 1, gpkg_table_item
        )
        gpkg_file_item.connectionChanged.connect(gpkg_table_item.setConnectionName)

        db_item = QgsProviderConnectionComboBox("postgres")
        self.tableWidget.setCellWidget(self.tableWidget.rowCount() - 1, 2, db_item)
        schema_item = QgsDatabaseSchemaComboBox("postgres", db_item.currentConnection())
        self.tableWidget.setCellWidget(self.tableWidget.rowCount() - 1, 3, schema_item)
        db_item.connectionChanged.connect(schema_item.setConnectionName)
        table_item = QLineEdit(gpkg_table_item.currentTable())
        self.tableWidget.setCellWidget(self.tableWidget.rowCount() - 1, 4, table_item)
        gpkg_table_item.tableChanged.connect(table_item.setText)

        if self.tableWidget.rowCount() == 1:
            self.tableWidget.resizeColumnsToContents()
        # set new row widget values from the last row values
        elif self.tableWidget.rowCount() > 1:
            gpkg_file = self.tableWidget.cellWidget(
                self.tableWidget.rowCount() - 2, 0
            ).currentConnection()
            gpkg_file_item.setConnection(gpkg_file)
            connection = self.tableWidget.cellWidget(
                self.tableWidget.rowCount() - 2, 2
            ).currentConnection()
            db_item.setConnection(connection)
            schema = self.tableWidget.cellWidget(
                self.tableWidget.rowCount() - 2, 3
            ).currentSchema()
            schema_item.setSchema(schema)

    def remove_row(self):
        self.tableWidget.removeRow(self.tableWidget.currentRow())

    def export_mapping(self):
        yaml_file, _ = QFileDialog.getSaveFileName(
            self, self.tr("YAML output"), filter="YAML (*.yaml)"
        )

        db_map = dict()
        for i in range(self.tableWidget.rowCount()):
            db_map["Table" + str(i)] = dict()
            db_map["Table" + str(i)]["gpkg_file"] = self.tableWidget.cellWidget(
                i, 0
            ).currentConnectionUri()
            db_map["Table" + str(i)]["gpkg_table"] = self.tableWidget.cellWidget(
                i, 1
            ).currentTable()

            connection = self.tableWidget.cellWidget(i, 2).currentConnection()
            db_map["Table" + str(i)]["connection_name"] = connection
            db_conn = QgsDataSourceUri(
                self.tableWidget.cellWidget(i, 2).currentConnectionUri()
            )
            if QSettings().value(
                "PostgreSQL/connections/{}/authcfg".format(connection), False
            ):
                db_conn.setUsername("")
                db_conn.setPassword("")
                db_conn.setAuthConfigId(
                    QSettings().value(
                        "PostgreSQL/connections/{}/authcfg".format(connection), False
                    )
                )
            db_map["Table" + str(i)]["db_conn"] = db_conn.uri(False)
            db_map["Table" + str(i)]["db_schema"] = self.tableWidget.cellWidget(
                i, 3
            ).currentSchema()
            db_map["Table" + str(i)]["db_table"] = self.tableWidget.cellWidget(
                i, 4
            ).text()

        with open(yaml_file, "w") as file:
            data = yaml.dump(db_map, file)

    def import_mapping(self):
        file, _ = QFileDialog.getOpenFileName(
            self, self.tr("YAML mapping"), filter="YAML (*.yaml)"
        )

        if file:
            with open(file, "r") as yamlfile:
                data = yaml.load(yamlfile, Loader=yaml.FullLoader)

            if data:
                self.tableWidget.clearContents()
                for i, d in enumerate(data.values()):
                    self.add_row()
                    gpkg_file_item = self.tableWidget.cellWidget(
                        self.tableWidget.rowCount() - 1, 0
                    )
                    for i in range(0, gpkg_file_item.count()):
                        if (
                            gpkg_file_item.itemData(
                                i, QgsProviderConnectionModel.RoleUri
                            )
                            == d["gpkg_file"]
                        ):
                            gpkg_file_item.setCurrentIndex(i)
                            gpkg_file_item.connectionChanged.emit(
                                gpkg_file_item.currentConnection()
                            )
                            break
                    gpkg_table_item = self.tableWidget.cellWidget(
                        self.tableWidget.rowCount() - 1, 1
                    )
                    gpkg_table_item.setTable(d["gpkg_table"])

                    db_item = self.tableWidget.cellWidget(
                        self.tableWidget.rowCount() - 1, 2
                    )
                    db_item.setCurrentIndex(db_item.findText(d["connection_name"]))
                    db_item.connectionChanged.emit(db_item.currentConnection())
                    schema_item = self.tableWidget.cellWidget(
                        self.tableWidget.rowCount() - 1, 3
                    )
                    schema_item.setSchema(d["db_schema"])
                    table_item = self.tableWidget.cellWidget(
                        self.tableWidget.rowCount() - 1, 4
                    )
                    table_item.setText(d["db_table"])

    @staticmethod
    def get_credentials(auth_id):
        """" Expand auth_id to get credentials"""
        print(auth_id)
        auth = QgsApplication.authManager()
        username, password = None, None
        if auth_id in auth.configIds():
            config = auth.loadAuthenticationConfig(
                auth_id, QgsAuthMethodConfig(), True
            )[1]
            credentials = dict(
                [tuple(kv.split(":::")) for kv in config.configString().split("|||")]
            )
            if "username" in credentials and "password" in credentials:
                return credentials["username"], credentials["password"]
