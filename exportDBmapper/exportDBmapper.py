# -----------------------------------------------------------------
# Copyright (C) 2021 Oslandia
# Conctact : sebastien.peillet@oslandia.com ; contact@oslandia.com
# -----------------------------------------------------------------
# Licensed under the terms of GNU GPL 2
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# ---------------------------------------------------------------------

import os
from PyQt5.QtWidgets import QAction, QWidget, QToolBar
from PyQt5.QtGui import QIcon
from PyQt5.QtCore import QSettings, QTranslator, QCoreApplication
from qgis.core import QgsApplication
from qgis.utils import showPluginHelp

from .gui.exportDBMapperDialog import ExportDBMapperDialog
from .provider.exportDBMapperProvider import ExportDBMapperProvider


class ExportDBMapper(QWidget):
    def __init__(self, iface):
        super(ExportDBMapper, self).__init__()
        self.iface = iface
        self.mapper_action = None
        self.toolbar = None
        self.exportDBMapperprovider = None

        locale = QSettings().value("locale/userLocale") or "en_USA"
        locale = locale[0:2]
        locale_path = os.path.join(
            os.path.dirname(__file__), "i18n", "exportdbmapper_{}.qm".format(locale)
        )
        if os.path.exists(locale_path):
            self.translator = QTranslator()
            self.translator.load(locale_path, "exportdbmapper")
            QCoreApplication.installTranslator(self.translator)
            print("TRANSLATION LOADED", locale_path)

    def initGui(self):
        self.initProcessing()

        self.toolbar = QToolBar(self.tr("exportdbmapper_toolbar"))
        self.toolbar.setObjectName("exportdbmapper_toolbar")
        self.iface.addToolBar(self.toolbar)

        self.mapper_action = QAction(
            QIcon(os.path.join(os.path.dirname(__file__), "icon.png")),
            "MappingEditor",
            self.iface.mainWindow(),
        )
        self.mapper_action.triggered.connect(self.configMapExport)

        self.action_help = QAction(
            QIcon(":/images/themes/default/mActionHelpContents.svg"),
            self.tr("Help"),
            self.iface.mainWindow(),
        )
        self.action_help.triggered.connect(
            lambda: showPluginHelp(filename="help/index")
        )

        self.iface.addPluginToDatabaseMenu(
            self.tr("ExportDBMapper"), self.mapper_action
        )
        self.iface.addPluginToDatabaseMenu(self.tr("ExportDBMapper"), self.action_help)
        self.toolbar.addAction(self.mapper_action)

    def initProcessing(self):
        self.exportDBMapperprovider = ExportDBMapperProvider()
        QgsApplication.processingRegistry().addProvider(self.exportDBMapperprovider)

    def unload(self):
        # unload action
        self.iface.removePluginMenu("ExportDBMapper", self.mapper_action)
        self.toolbar.setParent(None)
        self.mapper_action.setParent(None)
        del self.mapper_action

        # unload provider
        QgsApplication.processingRegistry().removeProvider(self.exportDBMapperprovider)

    def configMapExport(self):
        dlg = ExportDBMapperDialog(self.iface)
        dlg.exec_()
