<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="2.0" language="fr" sourcelanguage="">
<context>
    <name>ExportDBMapper</name>
    <message>
        <location filename="../exportDBmapper.py" line="46"/>
        <source>exportdbmapper_toolbar</source>
        <translation>barre_mappage_export</translation>
    </message>
    <message>
        <location filename="../exportDBmapper.py" line="69"/>
        <source>ExportDBMapper</source>
        <translation>ExportDBMapper</translation>
    </message>
    <message>
        <location filename="../exportDBmapper.py" line="57"/>
        <source>Help</source>
        <translation>Documentation</translation>
    </message>
</context>
<context>
    <name>ExportDBMapperProvider</name>
    <message>
        <location filename="../provider/exportDBMapperProvider.py" line="44"/>
        <source>ExportDBMapper</source>
        <translation>ExportDBMapper</translation>
    </message>
</context>
<context>
    <name>ExportGPKGToPGAlgorithm</name>
    <message>
        <location filename="../provider/gpkg2pg.py" line="65"/>
        <source>Export gpkg to pg</source>
        <translation>Export gpkg vers pg</translation>
    </message>
    <message>
        <location filename="../provider/gpkg2pg.py" line="72"/>
        <source>Tools</source>
        <translation>Outils</translation>
    </message>
    <message>
        <location filename="../provider/gpkg2pg.py" line="90"/>
        <source>Exportgpkg2pg is an algorithm that takes a yaml file created by the plugin to do batch export between GPKG fileand PG database.</source>
        <translation>Exportgpkg2pg est un algorithme qui utilise un fichier yaml pour réaliser un export mappée depuis un fichier GPKG vers une base PG.</translation>
    </message>
    <message>
        <location filename="../provider/gpkg2pg.py" line="100"/>
        <source>Input yaml</source>
        <translation>Fichier yaml</translation>
    </message>
    <message>
        <location filename="../provider/gpkg2pg.py" line="106"/>
        <source>Truncate table</source>
        <translation>Truncate les tables</translation>
    </message>
    <message>
        <location filename="../provider/gpkg2pg.py" line="167"/>
        <source> has been exported to </source>
        <translation> a été exportée vers </translation>
    </message>
    <message>
        <location filename="../provider/gpkg2pg.py" line="112"/>
        <source>Project output</source>
        <translation>Projet en sortie</translation>
    </message>
</context>
<context>
    <name>ExportPGToGPKGAlgorithm</name>
    <message>
        <location filename="../provider/pg2gpkg.py" line="65"/>
        <source>Export pg to gpkg</source>
        <translation>Export pg vers gpkg</translation>
    </message>
    <message>
        <location filename="../provider/pg2gpkg.py" line="72"/>
        <source>Tools</source>
        <translation>Outils</translation>
    </message>
    <message>
        <location filename="../provider/pg2gpkg.py" line="90"/>
        <source>Exportpg2gpkg is an algorithm that takes a yaml file created by the plugin to do batch export between PG database and GPKG file.</source>
        <translation>Exportpg2gpkg est un algorithme qui utilise un fichier yaml pour réaliser un export mappée depuis une base PG vers un fichier GPKG.</translation>
    </message>
    <message>
        <location filename="../provider/pg2gpkg.py" line="100"/>
        <source>Input yaml</source>
        <translation>Fichier yaml</translation>
    </message>
    <message>
        <location filename="../provider/pg2gpkg.py" line="106"/>
        <source>Truncate table</source>
        <translation>Truncate les tables</translation>
    </message>
    <message>
        <location filename="../provider/pg2gpkg.py" line="149"/>
        <source> has been exported to </source>
        <translation> a été exportée vers </translation>
    </message>
    <message>
        <location filename="../provider/pg2gpkg.py" line="111"/>
        <source>Project output</source>
        <translation>Projet en sortie</translation>
    </message>
</context>
<context>
    <name>Form</name>
    <message>
        <location filename="../gui/pgToGpkgWidget.ui" line="14"/>
        <source>Form</source>
        <translation>Formulaire</translation>
    </message>
    <message>
        <location filename="../gui/pgToGpkgWidget.ui" line="74"/>
        <source>GPKG file</source>
        <translation>Fichier GPKG</translation>
    </message>
    <message>
        <location filename="../gui/pgToGpkgWidget.ui" line="79"/>
        <source>GPKG table</source>
        <translation>Table GPKG</translation>
    </message>
    <message>
        <location filename="../gui/pgToGpkgWidget.ui" line="59"/>
        <source>PG connection</source>
        <translation>Connection PG</translation>
    </message>
    <message>
        <location filename="../gui/pgToGpkgWidget.ui" line="64"/>
        <source>PG schema</source>
        <translation>Schema PG</translation>
    </message>
    <message>
        <location filename="../gui/pgToGpkgWidget.ui" line="69"/>
        <source>PG table</source>
        <translation>Table PG</translation>
    </message>
    <message>
        <location filename="../gui/pgToGpkgWidget.ui" line="87"/>
        <source>Import YAML</source>
        <translation>Importer YAML</translation>
    </message>
    <message>
        <location filename="../gui/pgToGpkgWidget.ui" line="115"/>
        <source>Export</source>
        <translation>Export</translation>
    </message>
    <message>
        <location filename="../gui/pgToGpkgWidget.ui" line="94"/>
        <source>Append to an existing GPKG</source>
        <translation>Ajouter a un GPKG existant</translation>
    </message>
</context>
<context>
    <name>GPKGOutputSelector</name>
    <message>
        <location filename="../gui/gpkgOutputSelector.py" line="31"/>
        <source>GPKG output</source>
        <translation>Sortie GPKG</translation>
    </message>
</context>
<context>
    <name>GpkgToPgWidget</name>
    <message>
        <location filename="../gui/gpkgToPgWidget.py" line="70"/>
        <source>YAML output</source>
        <translation>Sortie YAML</translation>
    </message>
    <message>
        <location filename="../gui/gpkgToPgWidget.py" line="101"/>
        <source>YAML mapping</source>
        <translation>YAML</translation>
    </message>
</context>
<context>
    <name>MappingEditor</name>
    <message>
        <location filename="../gui/exportDBMapperDialog.ui" line="14"/>
        <source>MappingEditor</source>
        <translation>MappingEditor</translation>
    </message>
</context>
<context>
    <name>PgToGpkgWidget</name>
    <message>
        <location filename="../gui/pgToGpkgWidget.py" line="69"/>
        <source>YAML output</source>
        <translation>Sortie YAML</translation>
    </message>
    <message>
        <location filename="../gui/pgToGpkgWidget.py" line="105"/>
        <source>YAML mapping</source>
        <translation>YAML</translation>
    </message>
</context>
</TS>
