FORMS = ../gui/exportDBMapperDialog.ui \
../gui/gpkgToPgWidget.ui \
../gui/pgToGpkgWidget.ui
 
 
SOURCES= ../gui/exportDBMapperDialog.py \
../gui/gpkgOutputSelector.py \
../gui/gpkgToPgWidget.py \
../gui/pgToGpkgWidget.py \
../provider/pg2gpkg.py \
../provider/gpkg2pg.py \
../provider/exportDBMapperProvider.py \
../__init__.py \
../exportDBmapper.py

 
 
TRANSLATIONS = exportDBMapper_fr.ts 
