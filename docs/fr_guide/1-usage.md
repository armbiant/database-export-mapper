# Introduction

exportDBmapper est une extension QGIS 3 pour mapper des exports entre les bases de données PostgreSQL et les fichiers GPKG. Il est composé d'un éditeur d'export et d'une boîte à outils de traitement.

## Editeur d'export

L'éditeur d'export permet de définir les sources et destination d'export entre une base de donnée PostgreSQL et un fichier GPKG. Celui-ci est composé de deux onglets. Le premier est utile dans le cas d'un export depuis une base PG vers un ou plusieurs fichiers GPKG. Le deuxième est utile dans le cas d'un export depuis un ou plusieurs fichiers GPKG vers une base de donnée PG. Chaque onglet produit un fichier YAML contenant vos paramètres. Il est possible d'importer un fichier YAML dans la fenêtre pour modifier une configuration d'export déjà existant.

![mapping_editor1](/_static/images/mapping_editor1.png)

*Edition d'export de PG vers GPKG*

L'option *Ajouter a un GPKG existant* permet d'ajouter une table à un fichier GPKG existant au lieu d'écraser le fichier initial.

![mapping_editor2](/_static/images/mapping_editor2.png)

*Edition d'export de GPKG vers PG*


## Traitement d'export

Il existe deux traitements dans la boîte à outil.

![processing_toolbox](/_static/images/processing_toolbox.png)

Chacun des traitement attend un fichier YAML comme entrée et effectue l'export.
